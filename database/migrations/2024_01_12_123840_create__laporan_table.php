<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('laporan', function (Blueprint $table) {
            $table->id();
            $table->string("nama");
            $table->string("lokasi_kejadian");
            $table->text('deskripsi_kejadian');
            $table->enum('Stauts',['Sedang Diproses','Selesai']);
            $table->string('bukti');
            $table->datetime('tanggal_waktu_kejadian');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('_laporan');
    }
};

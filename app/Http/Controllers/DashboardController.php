<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\laporan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class DashboardController extends Controller
{
    public function index()
    {   
        $laporandata = laporan::count();
        $userdata = User::count();
        $laporan = laporan::all();
        return view('dashboard',['laporan_count'=>$laporandata,'user_count'=>$userdata,'laporan'=>$laporan]);
    }
    public function laporan()
    {
        $laporan = laporan::all();
        return view('laporan',['laporan' => $laporan]);
    }
    public function DashboardUser()
    {
        $user = User::where('role_id',2)->get();
        return view('user',['user'=>$user]);
    }
    public function edit($user_id)
    {   
        $result = User::findOrFail($user_id);
        return view('user-edit',['user' => $result]);
    }
    public function update(Request $request, User $user){
        $validated = $request->validate([
        'username' => 'required|unique:users|max:255',
        'password' => 'required|max:255',
    ]);
       $user->username  = $validated['username'];
       $user->password = $validated['password'];
       $user->save();
        Session::flash('status','Berhasil');
        Session::flash('message','Data Successfully Updated');
        return redirect()->route('user');

    }
    public function delete($user_id)
    {
        $result = User::findOrFail($user_id);
        return view('user-delete',['user' => $result]);
    }

    public function  destroy(Request $request ,User $user)
    {
        $user->delete();
        Session::flash('message','Data Successfully Deleted');
        return redirect()->route('user');

    }

     public function proses($laporan)
    {
        $result = laporan::findOrFail($laporan);
        return view('laporan-process',['laporan' => $result]);
    }
    public function prosess(laporan $laporan)
    {
        $laporan->Stauts = "Selesai" ;
        $laporan->save();
        Session::flash('message','Status Successfully update');
        return redirect()->route('laporan');
    }
}


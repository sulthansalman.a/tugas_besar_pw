<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\laporan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use File;

class LaporanApiController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $result = Laporan::all()->toJson(JSON_PRETTY_PRINT);
        return response($result, 200);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        if (Laporan::where('id', $id)->exists()) {
            $result = Laporan::find($id);
            File::delete($result->image);
            $result->delete();
            return response()->json([
                "message" => "laporan record deleted"
            ], 201);
        } else {
            return response()->json([
                "message" => "laporan not found"
            ], 404);
        }
    }
}

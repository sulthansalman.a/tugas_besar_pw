<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\laporan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Session;

class UserController extends Controller
{
    //
    public function index()
    {
        $result = User::findOrFail(Auth::user()->id);
        return view('profile',['user' => $result  ]);
        
    }
    public function laporan(Request $request)
    {   
           $user = Auth::user()->id;
            $laporan = laporan::where('user_id',$user)->get();
            return view('laporan-user')->with(['laporan'=>$laporan]);
    }
    public function add()
    {
        return view('laporan-add');
    }
    public function  store(Request $request)
    {
        $userid = (!Auth::guest()) ? Auth::user()->id : null ;
    $data['user_id'] = $userid;
    $data['nama'] = $request['nama'];
    $data['lokasi_kejadian'] = $request['lokasi_kejadian'];
    $data['deskripsi_kejadian'] = $request['deskripsi_kejadian'];
    $data['bukti'] = $request['bukti'];
    $data['tanggal_waktu_kejadian'] = $request['tanggal_waktu_kejadian'];
    laporan::create($data);
    return redirect()->route('laporan-user');
    }
    public function delete($laporan)
    {
        $result = laporan::findOrFail($laporan);
        return view('laporan-delete',['laporan' => $result]);
    }

    public function  destroy(Request $request ,laporan $laporan)
    {
        $laporan->delete();
        Session::flash('message','Data Successfully Deleted');
        return redirect()->route('laporan-user');

    }
     public function edit($laporan)
    {   
        $result = laporan::findOrFail($laporan);
        return view('laporan-edit',['laporan' => $result]);
    }
    public function update(Request $request, laporan $data){
      
       $userid = (!Auth::guest()) ? Auth::user()->id : null ;
    $data['user_id'] = $userid;
    $data['nama'] = $request['nama'];
    $data['lokasi_kejadian'] = $request['lokasi_kejadian'];
    $data['deskripsi_kejadian'] = $request['deskripsi_kejadian'];
    $data['bukti'] = $request['bukti'];
    $data['tanggal_waktu_kejadian'] = $request['tanggal_waktu_kejadian'];
    laporan::create($data);
    return redirect('laporan-user');
        

    }
    public function updateUser(Request $request){
        
        $validated = $request->validate([
        'username' => 'required|unique:users|max:255',
        'password' => 'required|max:255',
    ]);
    $user = Auth::user()->id;
    
       $user->username  = $validated['username'];
       $user->password = $validated['password'];
       $user->save();
        Session::flash('status','Berhasil');
        Session::flash('message','Data Successfully Updated');
        return redirect()->route('profile');

    }
}

<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Session;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        return view('login');
    }
    public function register(Request $request)
    {
        return view('register');
    }
    public function Authenticating(Request $request): RedirectResponse
    {
        $credentials = $request->validate([
            'username' => ['required'],
            'password' => ['required'],
        ]);
 
        if (Auth::attempt($credentials)) {
            //$request->session()->regenerate();
            
            //return redirect()->intended('dashboard');
            if(Auth::user()->role_id==1)
            {
                return redirect('dashboard');
            }
            if(Auth::user()->role_id==2)
            {
                return redirect('profile');
            }
        }
        Session::flash('status','invalid');
        Session::flash('message','Username or Password Incorrect');
        return redirect('/login');
    }
    public function logout(Request $request)
    {
        Auth::logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();
        return redirect('/login');
    }
    public function registerprocess(Request $request)
    {
         $validated = $request->validate([
        'username' => 'required|unique:users|max:255',
        'password' => 'required|max:255',
    ]);
       $user = User::create($request->all());
       return redirect('/login');
    }
}


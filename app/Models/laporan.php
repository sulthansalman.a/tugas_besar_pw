<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class laporan extends Model
{
    use HasFactory;
    protected $fillable =[
        'nama','lokasi_kejadian','deskripsi_kejadian','bukti','tanggal_waktu_kejadian','user_id'
    ];
    protected $attributes = [
        'Stauts' =>'Sedang Diproses'
    ];
}


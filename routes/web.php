<?php

use GuzzleHttp\Middleware;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\DashboardController;
use SebastianBergmann\CodeCoverage\Report\Html\Dashboard;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::middleware('guest')->group(function(){
    Route::get('login',[AuthController::class,'login']);
    Route::post('login',[AuthController::class,'Authenticating'])->name('login.Authenticating');
    Route::get('register',[AuthController::class,'register']);
    Route::post('register',[AuthController::class,'registerprocess']);
});
Route::middleware('auth')->group(function(){
    Route::get('dashboard',[DashboardController::class,'index'])->middleware('only_admin');
    Route::get('laporan',[DashboardController::class,'laporan'])->middleware('only_admin')->name('laporan');
    Route::get('user',[DashboardController::class,'DashboardUser'])->middleware('only_admin')->name('user');
    Route::get('user-edit/{user}',[DashboardController::class,'edit'])->middleware('only_admin')->name('user.edit');
    Route::post('user-edit/{user}', [DashboardController::class,'update'])->middleware('only_admin')->name('user.update');
    Route::get('user-delete/{user}',[DashboardController::class,'delete'])->middleware('only_admin');
    Route::get('user-destroy/{user}', [DashboardController::class,'destroy'])->name('user.destroy')->middleware('only_admin');
    Route::get('laporan-proses/{laporan}',[DashboardController::class,'proses'])->middleware('only_admin');
    Route::get('laporan-prosess/{laporan}',[DashboardController::class,'prosess'])->middleware('only_admin');

    Route::get('profile',[UserController::class,'index'])->middleware('only_user')->name('profile');
    Route::get('laporan_user',[UserController::class,'laporan'])->middleware('only_user')->name('laporan-user');
    Route::get('laporan-add',[UserController::class,'add'])->middleware('only_user');
    Route::post('laporan-add',[UserController::class,'store'])->middleware('only_user');
    Route::get('laporan-delete/{laporan}',[UserController::class,'delete'])->middleware('only_user');
    Route::get('laporan-destroy/{laporan}', [UserController::class,'destroy'])->name('laporan.destroy')->middleware('only_user');
    Route::get('laporan-edit/{laporan}',[UserController::class,'edit'])->middleware('only_user');
    Route::post('laporan-edit/{laporan}', [UserController::class,'update'])->name('laporan.update')->middleware('only_user');
    Route::post('profile', [UserController::class,'updateUser'])->name('profile.update')->middleware('only_user');
});

Route::get('home',[HomeController::class,'index']);
Route::get('logout',[AuthController::class,'logout']);


@extends('layout.mainlayout')
@section('title','Dashboard Admin')
    
@section('content')
    <h1>Welcome , {{Auth::user()->username}}</h1>
   <div class="row mt-5">
    <div class="col-lg-6">
        <div class="card-data">
            <div class="row">
                <div class="col-6"><i class="bi bi-person-fill"></i></div>
                <div class="col-6 d-flex flex-column justify-content-center align-items-end">
                    <div class = "card-desc">User</div>
                    <div class = "card-total">{{$user_count}}</div>
                </div>
            </div>
        </div>
        </div>
         <div class="col-lg-6">
        <div class="card-data">
            <div class="row">
                <div class="col-6"><i class="bi bi-clipboard-fill"></i></div>
                <div class="col-6 d-flex flex-column justify-content-center align-items-end">
                    <div class = "card-desc">Laporan</div>
                    <div class = "card-total">{{$laporan_count}}</div>
                </div>
            </div>
        </div>
        </div>
   </div>
   <div class="mt-5">
    <h2>Laporan</h2>
    <table class = "table colortext">
        <thead>
            <tr>
                <th>No.</th>
                <th>User</th>
                <th>Nama Laporan</th>
                <th>Lokasi Kejadian</th>
                <th>Deskripsi Kejadian</th>
                <th>Waktu dan Tanggal Kejadian</th>
                <th>Status</th>
                <th>Bukti</th>
                
            </tr>
        </thead>
        <tbody>
            @forelse ($laporan as $item)
                <tr>
                <th>{{$loop->iteration}}</th>
                <td>{{$item->user_id}}</td>
                <td>{{$item->nama}}</td>
                <td>{{$item->lokasi_kejadian}}</td>
                <td>{{$item->deskripsi_kejadian }}</td>
                <td>{{$item->tanggal_waktu_kejadian}}</td>
                <td>{{$item->Stauts}}</td>
                <td>{{$item->bukti}}</td>
                <td>
                    <a href="/laporan-proses/{{$item->id}}">proses</a>
                </td>
                </tr>
                @empty
                <td colspan="9" class="text-center">Tidak ada data...</td>
                @endforelse
        </tbody>
    </table>
   </div>
@endsection    
@extends('layout.mainlayout')
@section('title','Dashboard Admin')
    
@section('content')
<div class = " d-flex flex-column justify-content-center align-items-center">
            @if (session('status'))
            <div class="alert alert-danger">
                {{ session('message') }}
            </div>
            @endif
            <div class ="login">
                <div class="text-center textlog">
                    Edit User
                </div>
                
                <form action="{{route('user.update',['user' => $user->id])}}" method="POST">
                    @csrf
                    <div>
                        <label for="username" class="form-label">Username </label>
                        <input type="text" name="username" id ="username" class="form-control" value="{{$user->username}}" required>
                    </div>
                    <div>
                        <label for="password" class="form-label">Password</label>
                        <input type="text" name="password" id="password"class="form-control"  value="{{$user->password}}" required>
                    </div>
                    <div >
                        <button type="submit"class="btn btn-new form-control ">Update</button>
                    </div>
                </form>
                
            </div>
        </div>
@endsection   
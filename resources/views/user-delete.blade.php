@extends('layout.mainlayout')
@section('title','Dashboard Laporan')
    
@section('content')
    <h2>Are u want to  delete this User {{$user->username}}</h2>
    <div class="mt-5">
        <a href="/user-destroy/{{$user->id}}" class="btn btn-danger me-5">sure</a>
        <a href="/user" class="btn btn-primary">cancel</a>
    </div>
@endsection    
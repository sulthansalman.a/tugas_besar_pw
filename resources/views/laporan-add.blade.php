@extends('layout.mainlayout')
@section('title','Add Laporan User')

@section('content')

    <h1>Add New Laporan</h1>

    <div class ="mt-5 w-50">
        <form action="laporan-add"method="post">
            <div >
                @csrf
                <label for="nama" class="form-label">Nama Laporan</label>
                <input type="text" name="nama" id="nama" class="form-control">

                <label for="lokasi_kejadian" class="form-label">Lokasi Kejadian</label>
                <input type="text" name="lokasi_kejadian" id="lokasi_kejadian" class="form-control">

                 <label for="deskripsi_kejadian" class="form-label">Deskripsi Kejadian</label>
                <input type="text" name="deskripsi_kejadian" id="deskripsi_kejadian" class="form-control">

                 <label for="bukti" class="form-label">Bukti</label>
                <input type="file" name="bukti" id="bukti" class="form-control">

                 <label for="tanggal_waktu_kejadian" class="form-label">Tanggal Waktu Kejadian</label>
                <input type="date" name="tanggal_waktu_kejadian" id="tanggal_waktu_kejadian" class="form-control">
                <div class="mt-3">
                    <button class="btn btn-success" type="submit">Upload</button>
                </div>
            </div>
        </form>
    </div>
@endsection
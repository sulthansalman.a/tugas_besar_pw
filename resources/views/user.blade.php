@extends('layout.mainlayout')
@section('title','Dashboard Admin')
    
@section('content')
    <div class ="my-5">
        <h1>USER</h1>
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('message') }}
            </div>
            @endif
        <table class ="table colortext">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Username</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach  ($user as $item)
                <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{$item->username}}</td>
                    <td>
                        <a href="{{route('user.edit',['user' =>$item->id])}}">edit</a>
                        
                        <a href="/user-delete/{{$item->id}}">delete</a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection    
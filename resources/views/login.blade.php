@extends('layout.mainlayout')
@section('title','Homepage')
    
@section('content')
<div class = " d-flex flex-column justify-content-center align-items-center">
            @if (session('status'))
            <div class="alert alert-danger">
                {{ session('message') }}
            </div>
            @endif
            <div class ="login">
                <div class="text-center textlog">
                    LOGIN
                </div>
                
                <form action="{{route('login.Authenticating')}}" method="POST">
                    @csrf
                    <div>
                        <label for="username" class="form-label">Username </label>
                        <input type="text" name="username" id ="username" class="form-control" required>
                    </div>
                    <div>
                        <label for="password" class="form-label">Password</label>
                        <input type="password" name="password" id="password"class="form-control"required>
                    </div>
                    <div >
                        <button type="submit"class="btn btn-new form-control ">Login</button>
                    </div>
                    <div class="text-center">
                        <a href="/register">Signup</a>
                    </div>
                </form>
                
            </div>
        </div>
@endsection    

@extends('layout.mainlayout')
@section('title','Sign ')
    
@section('content')
<div class = "d-flex flex-column justify-content-center align-items-center">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class ="login">
                <div class="text-center textlog">
                    SIGNUP
                </div>
                <form action="" method="POST">
                    
                    @csrf
                    <div>
                        <label for="username" class="form-label">Username </label>
                        <input type="text" name="username" id ="username" class="form-control" >
                    </div>
                    <div>
                        <label for="password" class="form-label">Password</label>
                        <input type="Password" name="password" id="password"class="form-control" >
                    </div>
                    <div >
                        <button type="submit"class="btn btn-new form-control ">Signup</button>
                    </div>
                    <div class="text-center">
                        Have Account? <a href="login">Login</a>
                    </div>
                </form>
                
            </div>
        </div>
@endsection
    
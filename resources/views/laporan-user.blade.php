@extends('layout.mainlayout')
@section('title','Dashboard Laporan')
    
@section('content')
    <h2>Laporan</h2>
    <div class ="my-5 d-flex - justify-content-end">
        <a href="laporan-add" class="btn btn-primary ">Tambah Laporan</a>
    </div>
    <div class="my-5">
    
    
    
    <table class = "table colortext">
        <thead>
            <tr>
                <th>No.</th>
                <th>Nama Laporan</th>
                <th>Lokasi Kejadian</th>
                <th>Deskripsi Kejadian</th>
                <th>Waktu dan Tanggal Kejadian</th>
                <th>Status</th>
                <th>Bukti</th>
                <th>Action</th>
                
            </tr>
        </thead>
        <tbody>
            @forelse ($laporan as $item)
                <tr>
                <th>{{$loop->iteration}}</th>
                <td>{{$item->nama}}</td>
                <td>{{$item->lokasi_kejadian}}</td>
                <td>{{$item->deskripsi_kejadian }}</td>
                <td>{{$item->tanggal_waktu_kejadian}}</td>
                <td>{{$item->Stauts}}</td>
                <td>
                    <a href="/laporan-edit/{{$item->id}}">edit</a>
                    <a href="/laporan-delete/{{$item->id}}">delete</a>
                </td>
                </tr>
                @empty
                <td colspan="8" class="text-center">Tidak ada data...</td>
                @endforelse
        </tbody>
    </table>
   </div>
@endsection    
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Lapor Banyumas | @yield('title')</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.3/font/bootstrap-icons.min.css">
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
</head>

<body>
    <div class="main d-flex flex-column justify-content-between">
        <nav class="navbar navbar-expand-lg  colortext-background navbar-dark">
            <div class="container-fluid">
                <a class="navbar-brand" href="#">Lapor Banyumas</a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
                </button>
            </div>
        </nav>
        <div class="body-content h-100">
            <div class ="row g-0 h-100">
                <div class ="sidebar col-lg-2 collapse d-lg-block" id="navbarSupportedContent">
                    @if (Auth::user())
                        @if(Auth::user()->role_id==1)
                            <a href="/dashboard" @if(request()->route()->uri == 'dashboard')class="active"
                                @endif>Dashboard</a>
                            <a href="/laporan"@if(request()->route()->uri == 'laporan') class="active"
                                @endif>Laporan</a>
                            <a href="/user"@if(request()->route()->uri == 'user')class="active"
                                @endif>User</a>
                            <a href="/logout">Logout</a>
                            @else
                            <a href="/laporan_user" @if(request()->route()->uri == 'laporan_user')class="active"
                                @endif>Laporan</a>
                            <a href="/profile"@if(request()->route()->uri == 'profile')class="active"
                                @endif>Profile</a>
                            <a href="/logout">Logout</a>
                            @endif
                    @else
                            <a href="/home" @if(request()->route()->uri == 'home')class="active"
                                @endif>home</a>
                            <a href="/login"@if(request()->route()->uri == 'login')class="active"
                                @endif>login</a>
                            <a href="/register" @if(request()->route()->uri == 'register')class="active"
                                @endif>signup</a>
                        
                    @endif
                        
                            
                        
                    
                </div>
                <div class ="content p-5 col-lg-10">
                    @yield('content')
                </div>
            </div>
        </div>
    </div>
    <div>

    </div>
   <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4" crossorigin="anonymous"></script>
</body>
</html>